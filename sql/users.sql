-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.14 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table masqhero.users
CREATE TABLE IF NOT EXISTS `users` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `passhash` char(56) NOT NULL,
  `salt` char(56) NOT NULL,
  `email` varchar(75) NOT NULL,
  `gid` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  UNIQUE KEY `uid_username_email` (`username`,`email`,`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Dumping data for table masqhero.users: ~4 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT IGNORE INTO `users` (`uid`, `username`, `passhash`, `salt`, `email`, `gid`) VALUES
	(1, 'admin', 'b6289a3a13f58907b7d559166d1ff517328f13d39cdf45934b6b94d0', 'dbb7c13b2df9080e7a8dfba6cfa3e8441325354647ab52e5e973cfc7', 'admin@test.com', 7),
	(2, 'test1', 'ebba5a08108fc76362cd805417ab6ef0e586d91f92e754cc31b5353f', 'eb50fbfe1a2e2ff76ff10bab9d8e1f31d9183421fdbe5bbbcf9f0c1c', 'test1@test.com', 1),
	(3, 'test2', '66ff6c7c3cc40eb1506b06b0cf5c43a79370c6811914f89678f05fbc', 'a7a3bec73f54c8cf6c0c0205336ec62fe990836ab0354dcbb5d8b5ce', 'test2@test.com', 1),
	(4, 'test3', '471adf50c70f32272949cc8c98e47daec33374058de75573938923cd', 'a3ac9659e9c8f4e688221e178598a6b803ea1423715c7e4d7e55998a', 'test3@test.com', 1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
