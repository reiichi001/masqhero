-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.14 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table masqhero.entrydetail
CREATE TABLE IF NOT EXISTS `entrydetail` (
  `eid` int(11) NOT NULL,
  `epos` int(11) NOT NULL,
  `edenpai` int(11) NOT NULL DEFAULT '0',
  `ehumancount` int(11) NOT NULL DEFAULT '0',
  `epresentercount` int(11) NOT NULL DEFAULT '0',
  `eninjacount` int(11) DEFAULT '0',
  `epresentby` varchar(250) DEFAULT NULL,
  `econstructby` varchar(250) DEFAULT NULL,
  `esource` varchar(250) DEFAULT NULL,
  `egrouplist` varchar(250) DEFAULT NULL,
  `eworkmanshipjudge` tinyint(1) NOT NULL DEFAULT '0',
  `eaward` varchar(250) DEFAULT NULL,
  `ecertificatesneeded` int(11) DEFAULT NULL,
  PRIMARY KEY (`eid`),
  UNIQUE KEY `epos` (`epos`),
  UNIQUE KEY `eid` (`eid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table masqhero.entrydetail: ~0 rows (approximately)
/*!40000 ALTER TABLE `entrydetail` DISABLE KEYS */;
INSERT IGNORE INTO `entrydetail` (`eid`, `epos`, `edenpai`, `ehumancount`, `epresentercount`, `eninjacount`, `epresentby`, `econstructby`, `esource`, `egrouplist`, `eworkmanshipjudge`, `eaward`, `ecertificatesneeded`) VALUES
	(1, 1, 2, 3, 4, 5, 'Presented by stuff', 'Dowels Unlimited', 'Chinese Cartoons', 'CAE Masquerade and more', 1, 'Worst Entry', 1);
/*!40000 ALTER TABLE `entrydetail` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
