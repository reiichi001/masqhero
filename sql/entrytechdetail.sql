-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.14 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table masqhero.entrytechdetail
CREATE TABLE IF NOT EXISTS `entrytechdetail` (
  `eid` int(11) NOT NULL,
  `etfilename` varchar(50) DEFAULT NULL,
  `etaudiotype` varchar(10) DEFAULT NULL,
  `etneeds` text,
  `etbackdrops` tinyint(1) NOT NULL DEFAULT '0',
  `etsurprises` tinyint(1) NOT NULL DEFAULT '0',
  `etsurprisedetail` text,
  `etlightstart` text NOT NULL,
  `etaudiostart` text NOT NULL,
  `etpropsetup` text NOT NULL,
  `etpropstatus` tinyint(1) NOT NULL DEFAULT '0',
  `etmcenter` text,
  `etmcexit` text,
  PRIMARY KEY (`eid`),
  UNIQUE KEY `eid` (`eid`),
  UNIQUE KEY `etfilename` (`etfilename`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tech details for entries';

-- Dumping data for table masqhero.entrytechdetail: ~0 rows (approximately)
/*!40000 ALTER TABLE `entrytechdetail` DISABLE KEYS */;
INSERT IGNORE INTO `entrytechdetail` (`eid`, `etfilename`, `etaudiotype`, `etneeds`, `etbackdrops`, `etsurprises`, `etsurprisedetail`, `etlightstart`, `etaudiostart`, `etpropsetup`, `etpropstatus`, `etmcenter`, `etmcexit`) VALUES
	(1, 'fuck.mp3', 'mp3', 'We needs more dowels. And hot glue. ', 0, 1, 'We\'re not actually going to come on stage', 'Always on', 'Right at the beginning', 'We have no props', 0, 'This one time, at bandcamp...', '(deadpan) Please. Clap for them. They need the attention.');
/*!40000 ALTER TABLE `entrytechdetail` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
