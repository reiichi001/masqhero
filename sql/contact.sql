-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.14 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table masqhero.contact
CREATE TABLE IF NOT EXISTS `contact` (
  `eid` int(11) NOT NULL,
  `ecfirstname` varchar(50) DEFAULT NULL,
  `eclastname` varchar(50) DEFAULT NULL,
  `ecemail` varchar(256) DEFAULT NULL,
  `edaddress1` varchar(256) DEFAULT NULL,
  `ecaddress2` varchar(256) DEFAULT NULL,
  `eccity` varchar(256) DEFAULT NULL,
  `ecstate` varchar(256) DEFAULT NULL,
  `eczip` varchar(256) DEFAULT NULL,
  `eccountry` varchar(256) DEFAULT NULL,
  `ecphone` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`eid`),
  UNIQUE KEY `eid` (`eid`),
  FULLTEXT KEY `contact` (`ecfirstname`,`eclastname`,`ecemail`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='contact details for an entry';

-- Dumping data for table masqhero.contact: ~0 rows (approximately)
/*!40000 ALTER TABLE `contact` DISABLE KEYS */;
INSERT IGNORE INTO `contact` (`eid`, `ecfirstname`, `eclastname`, `ecemail`, `edaddress1`, `ecaddress2`, `eccity`, `ecstate`, `eczip`, `eccountry`, `ecphone`) VALUES
	(1, 'Test', 'Test', 'test@test.com', '123 Test', 'Apt D', 'City', 'CA', '99999', 'USA', '123-456-7890');
/*!40000 ALTER TABLE `contact` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
