-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.14 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table masqhero.entry
CREATE TABLE IF NOT EXISTS `entry` (
  `eseason` varchar(10) NOT NULL,
  `eid` int(11) NOT NULL,
  `ename` varchar(150) NOT NULL,
  `edivision` varchar(20) NOT NULL,
  `egroupstatus` int(11) NOT NULL,
  `erequirements` int(11) NOT NULL,
  PRIMARY KEY (`eid`),
  UNIQUE KEY `eid` (`eid`),
  KEY `eseason` (`eseason`),
  FULLTEXT KEY `ename` (`ename`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table masqhero.entry: ~9 rows (approximately)
/*!40000 ALTER TABLE `entry` DISABLE KEYS */;
INSERT IGNORE INTO `entry` (`eseason`, `eid`, `ename`, `edivision`, `egroupstatus`, `erequirements`) VALUES
	('ALATEST', 1, 'Some group', 'Master', 1, 5),
	('ALATEST', 2, 'Another group', 'Novice', 2, 3),
	('ALATEST', 3, 'Group 3', 'Master', 0, 0),
	('ALATEST', 4, 'Group 4', 'Master', 3, 11),
	('ALATEST', 5, 'Fifth Group', 'Novice', 4, 43),
	('ALATEST', 6, 'All Done', 'Master', 1, 63),
	('ALATEST', 7, 'Maybe', 'Novice', 2, 57),
	('ALATEST', 8, 'People', 'Journeyman', 1, 37),
	('ALATEST', 9, 'More People', 'First Masquerade', 2, 55),
	('ALATEST', 10, 'Even More People', 'Journeyman', 3, 41),
	('ALATEST', 11, 'Dokidoki I\'m too old for this', 'First Masquerade', 4, 23);
/*!40000 ALTER TABLE `entry` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
