-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.14 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table masqhero.entrystatus
CREATE TABLE IF NOT EXISTS `entrystatus` (
  `eid` int(11) NOT NULL,
  `edcontactstatus` int(11) DEFAULT NULL,
  `edtechstatus` int(11) DEFAULT NULL,
  `edcheckinstatus` int(11) DEFAULT NULL,
  `edwaiverstatus` int(11) DEFAULT NULL,
  `edaudiostatus` int(11) DEFAULT NULL,
  `eddocumentationstatus` int(11) DEFAULT NULL,
  PRIMARY KEY (`eid`),
  UNIQUE KEY `eid` (`eid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='things that should be pulled up on a per-entry basis';

-- Dumping data for table masqhero.entrystatus: ~0 rows (approximately)
/*!40000 ALTER TABLE `entrystatus` DISABLE KEYS */;
INSERT IGNORE INTO `entrystatus` (`eid`, `edcontactstatus`, `edtechstatus`, `edcheckinstatus`, `edwaiverstatus`, `edaudiostatus`, `eddocumentationstatus`) VALUES
	(1, 0, 0, 0, 0, 0, 0);
/*!40000 ALTER TABLE `entrystatus` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
