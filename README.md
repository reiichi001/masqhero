# MasqHero

(Link the google doc here)

### Requirements:
 1. A webserver with php support
 2. MySQL Server

https://www.mamp.info/en/
http://www.wampserver.com/en/

### Setup:
1. Install any required componants
2. Copy the files from www to your webserver's directory for html files. (htdoc, www, etc)
3. Import the SQL files into your MySQL database server.
On windows, there's a handy SQL import script that can be adjusted to your environment to automate this.
4. Adjust the config.php files to match your database server credentials (host, database/schema, username, password)

### Basic ideas:
1. Header.php, Template.php, and nologin.php are the base page templates. Design should be done on these files, which we'll then use as we build additional pages for the system. Images and CSS would likely benefit from being placed in subfolders and then referenced, to keep a clean design and to prevent excessive inline styling.
2. Index.php will be the front-facing view for the system. All users, whether administrator or not, should start here to login and generate a session. Permissions will then be handled based on user group permissions. (WIP)
3. There are two test users currently implemented, but their permissions and groups have not been set at this time. admin/admin and test1/test1 are their credentials.