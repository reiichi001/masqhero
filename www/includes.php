<?php
/**
 * Created by PhpStorm.
 * User: Robert
 * Date: 3/21/2018
 * Time: 3:19 PM
 */


function getCSS()
{
    echo
        '<!-- Bootstrap -->
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/aos.css">
        <link rel="stylesheet" href="assets/css/Navigation-Clean.css">
        <link rel="stylesheet" href="assets/css/Navigation-with-Button.css">
        <link rel="stylesheet" href="assets/css/Navigation-with-Search.css">
        <link rel="stylesheet" href="assets/css/styles.css">
        <!-- jasny tables-->
        <!-- <link rel="stylesheet" href="assets/css/jasny-bootstrap.min.css" media="screen">
        <!-- datatables-->
        <link rel="stylesheet" type="text/css" href="assets/css/datatables.min.css"/>
        ';
}

function getScripts()
{
    echo
        '<!-- Bootstrap requirements -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/js/bs-animation.js"></script>
        <script src="assets/js/aos.js"></script>
        <!-- Jasny table requirements -->
        <!-- <script src="assets/js/jasny-bootstrap.min.js"></script>
        <!-- Datatables requirements -->
        <script type="text/javascript" src="assets/js/datatables.min.js"></script>
        ';
}
