<?php
/**
 * Created by PhpStorm.
 * User: Robert
 * Date: 2/20/2018
 * Time: 11:21 PM
 */

session_start();
?>
<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>MasqHero User Administration</title>
	</head>
	<body>
		<?php include("header.php"); ?>
		<div class="info">
			<p>
				User <?php echo $_SESSION['username']?> created successfully.
			</p>
			<p>
				<a href="login.php">Login to MasqHero</a>
			</p>
		</div>
	</body>
</html>

<?php
session_unset();
session_destroy();