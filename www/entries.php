<?php
include ("includes.php");
include("inc/user/auth.php");
session_start();
?>

<?php
//PHP functions start here
if(!empty($_POST["logout"])) { logout(); }

//PHP functions on the page end here
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>MasqedHero Entries <?php if (isset($_SESSION['season'])) echo $_SESSION['season'] ?></title>
    <?php getCSS(); ?>

    <?php getScripts(); ?>

    <?php // load our initial side panel ?>
    <script>
        xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {

                document.getElementById("entrydetail").innerHTML = this.responseText;
            }
        }
        xmlhttp.open("POST", "inc/entries/entrystats.php", true);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.send();
    </script>


    <?php // load our entry table ?>
    <script type="text/javascript" language="javascript">

        //grab a dependency we need
        $.getScript("assets/js/status.js");

        //update the site panel as we click on entries
        function reloadPanel(eid)
        {
            if (eid.length == 0 || eid === 0)
            {
                document.getElementById("entrydetail").innerHTML = "Unable to load entry";
                return;
            } else
            {
                xmlhttp = new XMLHttpRequest();
                xmlhttp.onreadystatechange = function()
                {
                    if (this.readyState == 4 && this.status == 200)
                    {
                        document.getElementById("entrydetail").innerHTML = this.responseText;
                    }
                };
                xmlhttp.open("POST", "inc/entries/entrydetail.php", true);
                xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                xmlhttp.send("eid="+eid);
            }
        }

        $(document).ready(function() {
            var table = $('#entriestable').DataTable( {
                "processing": true,
                "serverSide": false,
                "ajax": {
                    "url": "inc/entries/listentries.php",
                    "type": "POST"
                },
                "columns": [
                    { "data": "eid" },
                    { "data": "ename" },
                    { "data": "edivision" },
                    { "data": "egroupstatus" },
                    { "data": "erequirements" },
                    { "data": null}
                ],
                "columnDefs": [
                    {
                        // The `data` parameter refers to the data for the cell (defined by the
                        // `data` option, which defaults to the column being worked with, in
                        // this case `data: 0`.
                        "render": function ( data, type, row )
                        {
                            return processGroupStatus(data);
                        },
                        "targets": 3
                    },
                    {
                        "render": function ( data, type, row )
                        {
                            return processRequirementsStatus(data);
                        },
                        "targets": 4
                    },
                    {
                        "targets": -1,
                        //"data": null,
                        "defaultContent": "<button>View Detail</button>"
                    }

                ]
            } );

            $('#entriestable tbody').on( 'click', 'button', function () {
                var data = table.row( $(this).parents('tr') ).data();
                reloadPanel(data['eid']);
            } );
        } );
    </script>


</head>

<body>



<div>
    <nav class="navbar navbar-default navigation-clean-button">
        <div class="container">
            <div class="navbar-header"><a class="navbar-brand" href="#">MasqedHero</a>
                <button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navcol-1"><span
                            class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span
                            class="icon-bar"></span><span class="icon-bar"></span></button>
            </div>
            <div class="collapse navbar-collapse" id="navcol-1">
                <?php
                if (checkLogin() && $_SESSION['gid'] === admin) {
                    ?>
                    <ul class="nav navbar-nav">
                        <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"
                                                href="#">Reports<span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li role="presentation"><a href="#">Problems to address</a></li>
                                <li role="presentation"><a href="#">Judge's Book</a></li>
                                <li role="presentation"><a href="#">Tech Book</a></li>
                                <li role="presentation"><a href="#">MC Book</a></li>
                                <li role="presentation"><a href="#">Order List</a></li>
                                <li role="presentation"><a href="#">Post-Con Report</a></li>
                            </ul>
                        </li>
                    </ul>
                    <form class="navbar-text navbar-right actions" action="" method="post" id="frmLogout">
                        Welcome, <?php echo $_SESSION['username'] ?>
                        <div class="clearfix visible-xs"><br/></div>
                        <input class="btn btn-default action-button" role="button" type="submit" name="logout"
                               value="Logout" style="align: right">
                    </form>
                    <ul class="nav navbar-nav"></ul>
                    <?php
                } else if ( checkLogin() ) {
                    ?>
                    <ul class="nav navbar-nav"></ul>
                    <form class="navbar-text navbar-right actions" action="" method="post" id="frmLogout">
                        Welcome, <?php echo $_SESSION['username'] ?>
                        <div class="clearfix visible-xs"><br/></div>
                        <input class="btn btn-default action-button" role="button" type="submit" name="logout"
                               value="Logout" style="align: right">
                    </form>
                    <ul class="nav navbar-nav"></ul>
                    <?php
                } else {
                    ?>
                    <ul class="nav navbar-nav"></ul>
                    <p class="navbar-text navbar-right actions"><a class="btn btn-default action-button" role="button"
                                                                   href="login.php">Login</a></p>
                    <ul class="nav navbar-nav"></ul>
                    <?php
                }
                ?>

            </div>
        </div>
    </nav>
</div>

<?php
if ( checkLogin() && $_SESSION['gid'] === admin) {
    ?>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Displaying all entries for: <?php echo $_SESSION['season'] ?></h3>
        </div>
        <div class="panel-body">
            <div class="row" style="width:100%;">
                <div class="col-lg-8 col-md-8 show" >
                    <p id="listentries"></p>

                    <table id="entriestable" class="table table-striped table-bordered table-hover display" style="width:100%" >
                        <thead>
                        <tr>
                            <th>Entry ID</th>
                            <th>Entry Name</th>
                            <th>Division</th>
                            <th>Group Status</th>
                            <th>Status2</th>
                            <th></th>

                        </tr>
                        </thead>
                        <!-- <tbody data-link="row" class="rowlink"> -->
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <div class="col-lg-4 col-md-4" data-aos="slide-left">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Event Details</h3>
                        </div>
                        <div class="panel-body">
                            <p id="entrydetail">
                                This section should populate with some counts while we don't have an entry selected.

                                Otherwise, it should show a nice panel with all the entry details, and later allow editing.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
} else if ( checkLogin() ) {
    echo "Logged in, but nothing to do yet.";
} else {
    include("nologin.php");
}
?>


</body>

</html>