function processGroupStatus(statusvar){
    if (statusvar === 0) //default value / no status
    {
        return '<span class="errorMessage">ERROR: No Status</span>';
    }
    else if (statusvar === 1) // Confirmed
    {
        return 'Confirmed';
    }
    else if (statusvar === 2) // Waitlist
    {
        return 'Waitlist';
    }
    else if (statusvar === 3) // Pending Approval
    {
        return 'Pending Approval';
    }
    else if (statusvar === 4) // Dropped
    {
        return 'Dropped';
    }
    else
    {
        return '<span class="errorMessage">ERROR: Invalid Status</span>';
    }
}

function processRequirementsStatus(statusvar){

    const TECH_FORM_SENT = 1;
    const TECH_FORM_RETURNED = 2;
    const CHECKED_IN = 4;
    const WAIVER_REC = 8;
    const AUDIO_REC = 16;
    const DOC_REC = 32;

    //make this flag prettier
    //there are 16 options
    //000000 (0) = nothing done
    //000001 (1) = tech form sent
    //000010 (2) = tech form received
    //000100 (4) = checked in
    //001000 (8) = waiver received
    //010000 (16) = audio received
    //100000 (32)= documentation received

    var status = '';

    if (!(statusvar & TECH_FORM_SENT))
    {
        return "Tech form not sent.";
    }

    if (!(statusvar & TECH_FORM_RETURNED))
    {
        status += "Tech";
    }

    if (!(statusvar & CHECKED_IN))
    {
        if (status.length > 0) status += ', ';
        status += "Check-in";
    }

    if (!(statusvar & WAIVER_REC))
    {
        if (status.length > 0) status += ', ';
        status += "Waiver";
    }

    if (!(statusvar & AUDIO_REC))
    {
        if (status.length > 0) status += ', ';
        status += "Audio";
    }

    if (!(statusvar & DOC_REC))
    {
        if (status.length > 0) status += ', ';
        status += "Documentation";
    }

    if (status === '')
    {
        return 'Ready';
    }
    else return status;
}