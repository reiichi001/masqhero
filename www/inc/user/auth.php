<?php
/**
 * Created by PhpStorm.
 * User: Robert
 * Date: 3/21/2018
 * Time: 12:08 PM
 */
include ("inc/user/group.php");
include ("inc/user/season.php");

function AuthUser($dataConnection, $username, $password)
{


    $statement = $dataConnection->prepare("SELECT uid, passhash, salt FROM users WHERE username = ?");
    if(!$statement)
    {
        throw new Exception(__FUNCTION__ . " failed: " . $dataConnection->error);
    }

    try
    {
        $uid = 0;
        $storedPasshash = "";
        $salt = "";

        $statement->bind_param('s', $username);
        if(!$statement->execute())
        {
            throw new Exception(__FUNCTION__ . " failed.");
        }

        $statement->bind_result($uid, $storedPasshash, $salt);
        if(!$statement->fetch())
        {
            throw new Exception("Incorrect username.");
        }

        $saltedPassword = $password . $salt;
        $hashedPassword = hash("sha224", $saltedPassword);

        if($hashedPassword !== $storedPasshash)
        {
            throw new Exception("Incorrect password.");
        }

        return $uid;
    }
    finally
    {
        $statement->close();
    }
}

function setLoginSession($dataConnection)
{
    try
    {
        $username = trim($_POST["username"]);
        $password = trim($_POST["password"]);



        $uid = AuthUser($dataConnection, $username, $password);
        $gid = GetUserGroup($dataConnection, $uid);
        $season  = GetSeasonForUser($dataConnection, $uid);

        if ($uid > 0)
        {
            $_SESSION['uid'] = $uid;
            $_SESSION['gid'] = $gid;
            $_SESSION['season'] = $season;
            $_SESSION['username'] = trim($_POST["username"]);
        }
        else
        {
            throw new Exception("Login failed");
        }


    }
    catch (Exception $e) {
        return $loginUserError = $e->getMessage();
    }
}

function checkLogin()
{
    $logged_in = false;

    if (isset($_SESSION['uid']) && isset($_SESSION['gid']))
    {
        $logged_in = true;
    }

    return $logged_in;
}

function logout()
{
    session_unset();
    session_destroy();
    header("location: index.php");
}