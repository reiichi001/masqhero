<?php

function CheckUserCreateInput($databaseConnection)
{
    $username 		= trim($_POST["username"]);
    $password 		= trim($_POST["password"]);
    $repeatpassword	= trim($_POST["repeatpassword"]);
    $email			= trim($_POST["email"]);

    try {
        if(empty($username))
        {
            throw new Exception("You must enter an username.");
        }

        if(empty($password))
        {
            throw new Exception("You must enter a password.");
        }

        if($password !== $repeatpassword)
        {
            throw new Exception("Repeated password doesn't match with entered password.");
        }

        if(empty($email) || !filter_var($email, FILTER_VALIDATE_EMAIL))
        {
            throw new Exception("You must enter a valid e-mail address.");
        }

        $salt = GenerateRandomSha224();
        $saltedPassword = $password . $salt;
        $hashedPassword = hash("sha224", $saltedPassword);

        InsertUser($databaseConnection, $username, $hashedPassword, $salt, $email);
    }
    catch (Exception $e)
    {
        //pass the exception back up the chain if we're not going to handle it gracefully
        throw $e;
    }

}

function InsertUser($dataConnection, $username, $passhash, $salt, $email)
{
    {
        $gid = 1;

        $statement = $dataConnection->prepare("INSERT INTO users (username, passhash, salt, email, gid) VALUES (?, ?, ?, ?, $gid)");
        if(!$statement)
        {
            throw new Exception(__FUNCTION__ . " failed: " . $dataConnection->error);
        }

        //try to add the user with default group
        try
        {
            $statement->bind_param('ssss', $username, $passhash, $salt, $email);

            if(!$statement->execute())
            {
                throw new Exception(__FUNCTION__ . " failed.");
            }
        }
        finally
        {
            $statement->close();
        }
    }
}