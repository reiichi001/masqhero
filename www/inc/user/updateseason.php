<?php
include(dirname(__DIR__) . DIRECTORY_SEPARATOR  . "database.php");
include("season.php");
session_start();

$query = "UPDATE users SET seasonid=? WHERE uid=?";

$statement = $g_databaseConnection->prepare($query);
if(!$statement)
{
    throw new Exception(__FUNCTION__ . " failed: " . $g_databaseConnection->error);
}

try
{

    $uid = $seasonid = -1;

    if (isset($_SESSION['uid']) && $_SESSION['uid'] > 0)
    {
        $uid = (int)$_SESSION['uid'];
    }

    if (isset($_POST['seasonid']) && $_POST['seasonid'] >= 0)
    {
        $seasonid = (int)$_POST['seasonid'];
    }

    /*
    echo var_dump($_POST);
    echo $query . "<br>";
    echo $uid . "<br>";
    echo $seasonid . "<br>";
    die();
    */
    $statement->bind_param('ii', $seasonid, $uid);

    if($statement->execute())
    {
        //updated DB, update the session
        $_SESSION['season'] = GetSeasonForUser($g_databaseConnection, $uid);

    }
    else
    {
        //DB not updated, don't touch the session
        throw new Exception(__FUNCTION__ . " failed.");
    }



    //echo "season set to: " . $season;

    if(isset($_REQUEST["destination"])){
        header("Location: {$_POST["destination"]}");
    }else if(isset($_SERVER["HTTP_REFERER"])){
        header("Location: {$_SERVER["HTTP_REFERER"]}");
    }else{
       //all else fails, send them home
        header("Location: index.php");
    }

}
finally
{
    $statement->close();
}