<?php
/**
 * Created by PhpStorm.
 * User: Robert
 * Date: 3/21/2018
 * Time: 12:09 PM
 */

const admin = 7;
const user = 0;

function GetUserGroup ($dataConnection, $uid)
{
    $statement = $dataConnection->prepare("SELECT gid FROM users WHERE uid = ?");
    if(!$statement)
    {
        throw new Exception(__FUNCTION__ . " failed: " . $dataConnection->error);
    }

    $gid = null;
    try
    {
        $statement->bind_param('i', $uid);
        if(!$statement->execute())
        {
            throw new Exception(__FUNCTION__ . " failed.");
        }

        $statement->bind_result($gid);
        if(!$statement->fetch())
        {
            throw new Exception("Incorrect username.");
        }


        return $gid;
    }
    finally
    {
        $statement->close();
    }
}