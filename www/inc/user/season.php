<?php
/**
 * Created by PhpStorm.
 * User: Robert
 * Date: 3/21/2018
 * Time: 12:09 PM
 */
function GetSeasonForUser($dataConnection, $uid)
{
    $sql = "SELECT seasonname FROM users, seasons WHERE uid = ? AND users.seasonid=seasons.seasonid";
    $statement = $dataConnection->prepare($sql);

    if (!$statement) {
        throw new Exception(__FUNCTION__ . " failed: " . $dataConnection->error);
    }

    $season = null;
    try {
        $statement->bind_param('i', $uid);
        if (!$statement->execute()) {
            throw new Exception(__FUNCTION__ . " failed.");
        }

        $statement->bind_result($season);
        if (!$statement->fetch()) {
            throw new Exception("Incorrect username.");
        }


        return $season;
    } finally {
        $statement->close();
    }
}

function GetSeasonList($dataConnection)
{
    $sql = "SELECT seasonid, seasonname FROM seasons ORDER BY seasonname ASC";
    $seasons = array();


    if ($results = $dataConnection->query($sql))
    {
        while ( $row = $results->fetch_assoc())
        {
            $seasons[] = $row;
        }

        $results->free();
    }

    return $seasons;
}