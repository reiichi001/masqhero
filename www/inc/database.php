<?php
include(dirname(__DIR__) . DIRECTORY_SEPARATOR ."inc/config.php");

$g_databaseConnection = OpenDB($hostname, $dbuser, $dbpass, $dbname);

//Helper functions used only with db calls
function GenerateRandomSha224()
{
    mt_srand(microtime(true) * 100000 + memory_get_usage(true));
    return hash("sha224", uniqid(mt_rand(), true));
}

//Database functions
function OpenDB($hostname, $dbuser, $dbpass, $dbname)
{
    $mysqli = new mysqli($hostname, $dbuser, $dbpass, $dbname);

    if ($mysqli->connect_error) {
        die('Connect Error: ' . $mysqli->connect_error);
    } else {
        //echo "Connection successful<br><br>";
    }

    return $mysqli;
}
