<?php

include(dirname(__DIR__) . DIRECTORY_SEPARATOR  . "database.php");
include(dirname(__DIR__) . DIRECTORY_SEPARATOR  . "entries" . DIRECTORY_SEPARATOR  . "status.php");
header("Content-Type: application/json; charset=UTF-8");

session_start();

$query = "SELECT ename, edivision, egroupstatus, erequirements FROM entry WHERE eseason=? AND eid=?";

$statement = $g_databaseConnection->prepare($query);
if(!$statement)
{
    throw new Exception(__FUNCTION__ . " failed: " . $g_databaseConnection->error);
}

try
{

    $eid = 0;
    if (isset($_POST['eid']) && $_POST['eid'] > 0)
    {
        $eid = (int)$_POST['eid'];
    }
    $statement->bind_param('si', $_SESSION['season'], $eid);

    if(!$statement->execute())
    {
        throw new Exception(__FUNCTION__ . " failed.");
    }


    $result = $statement->get_result();

    $data = array();

    while ($row = $result->fetch_assoc())
    {
        $data[] = $row;
    }


    echo (json_encode($data));

}
finally
{
    $statement->close();
}