<?php
/**
 * Created by PhpStorm.
 * User: Robert
 * Date: 2/27/2018
 * Time: 11:32 PM
 */

const TECH_FORM_SENT = 1;
const TECH_FORM_RETURNED = 2;
const CHECKED_IN = 4;
const WAIVER_REC = 8;
const AUDIO_REC = 16;
const DOC_REC = 32;
const READY = 63;

//server-side status number -> string
function processGroupStatus($statusvar)
{
    if ($statusvar == 0) //default value / no status
    {
        return '<span class="errorMessage">ERROR: No Status</span>';
    }
    else if ($statusvar == 1) // Confirmed
    {
        return 'Confirmed';
    }
    else if ($statusvar == 2) // Waitlist
    {
        return 'Waitlist';
    }
    else if ($statusvar == 3) // Pending Approval
    {
        return 'Pending Approval';
    }
    else if ($statusvar == 4) // Dropped
    {
        return 'Dropped';
    }
    else return '<span class="errorMessage">ERROR: Invalid Status</span>';;
}

//serverside number -> string
function processRequirementsStatus($statusvar)
{
    //make this flag prettier
    //there are 16 options
    //000000 (0) = nothing done
    //000001 (1) = tech form sent
    //000010 (2) = tech form received
    //000100 (4) = checked in
    //001000 (8) = waiver received
    //010000 (16) = audio received
    //100000 (32)= documentation received

    $status = '';

    if (!($statusvar & TECH_FORM_SENT))
    {
        return "Tech form not sent.";
    }

    if (!($statusvar & TECH_FORM_RETURNED))
    {
        $status .= "Tech";
    }

    if (!($statusvar & CHECKED_IN))
    {
        if (!empty($status)) $status .= ', ';
        $status .= "Check-in";
    }

    if (!($statusvar & WAIVER_REC))
    {
        if (!empty($status)) $status .= ', ';
        $status .= "Waiver";
    }

    if (!($statusvar & AUDIO_REC))
    {
        if (!empty($status)) $status .= ', ';
        $status .= "Audio";
    }

    if (!($statusvar & DOC_REC))
    {
        if (!empty($status)) $status .= ', ';
        $status .= "Documentation";
    }



    if ($statusvar === 63 || empty($status))
    {
        return 'Ready';
    }
    else return $status;
}

//serverside string -> array of valid numbers
function getStatusNum($statusstring)
{
    $results = array();
    if (stripos("No Status", $statusstring) !== false) //default value / no status
    {
        $results[] = 0;
    }
    if (stripos("Confirmed", $statusstring) !== false) // Confirmed
    {
        $results[] = 1;
    }
    if (stripos("Waitlist", $statusstring) !== false) // Waitlist
    {
        $results[] = 2;
    }
    if (stripos("Pending Approval", $statusstring) !== false) // Pending Approval
    {
        $results[] = 3;
    }
    if (stripos("Dropped", $statusstring) !== false) // Dropped
    {
        $results[] = 4;
    }

    return $results;
}

function isValidReqNum($i)
{
    if ($i === 0 || ($i % 2 === 1)) return true;
    else return false;
}

function getTechFormValidNum()
{
    $results = array();

    $results[] = 0;

    return $results;
}

function getTechRecValidNum()
{
    $results = array();

    for ($k = 1; $k<63; $k+=2)
    {
        //echo $k;
        if ( (TECH_FORM_SENT & $k) && ( TECH_FORM_RETURNED & $k) && isValidReqNum($k) )
        {
            //$results[] = $k;
        }
        else
        {
            $results[] = $k;
        }
    }

    return $results;
}

function getCheckInValidNum()
{
    $results = array();

    for ($k = 1; $k<63; $k+=2)
    {
        //echo $k;
        if ( (TECH_FORM_SENT & $k) && ( CHECKED_IN & $k) && isValidReqNum($k) )
        {
            //$results[] = $k;
        }
        else
        {
            $results[] = $k;
        }
    }

    return $results;
}

function getWaiverValidNum()
{
    $results = array();

    for ($k = 1; $k<63; $k+=2)
    {
        //echo $k;
        if ( (TECH_FORM_SENT & $k) && ( WAIVER_REC & $k) && isValidReqNum($k) )
        {
            //$results[] = $k;
        }
        else
        {
            $results[] = $k;
        }
    }

    return $results;
}

function getAudioValidNum()
{
    $results = array();

    for ($k = 1; $k<63; $k+=2)
    {
        //echo $k;
        if ( (TECH_FORM_SENT & $k) && ( AUDIO_REC & $k) && isValidReqNum($k) )
        {
            //$results[] = $k;
        }
        else
        {
            $results[] = $k;
        }
    }

    return $results;
}

function getDocumentationValidNum()
{
    $results = array();

    for ($k = 1; $k<63; $k+=2)
    {
        //echo $k;
        if ( (TECH_FORM_SENT & $k) && ( DOC_REC & $k) && isValidReqNum($k) )
        {
            //$results[] = $k;
        }
        else
        {
            $results[] = $k;
        }
    }

    return $results;
}

function getReadyValidNum()
{
    $results = array();

    $results[] = READY;

    return $results;
}

//serverside string -> array of valid numbers
function getReqNum($reqstring)
{
    $results = array();
    if (stripos("Tech form not sent", $reqstring) !== false) //default value / no status
    {
        $results = array_merge($results, getTechFormValidNum());
    }
    if (stripos("Tech", $reqstring) !== false) //add all numbers for TECH_FORM_RETURNED
    {
        $results = array_merge($results, getTechRecValidNum());
    }
    if ( (stripos("Check-in", $reqstring) !== false)
        || (stripos("Check in", $reqstring) !== false)
        || (stripos("Checked-in", $reqstring) !== false)
        || (stripos("Checked in", $reqstring) !== false) ) //add all numbers for CHECKED_IN
    {
        $results = array_merge($results, getCheckInValidNum());
    }
    if (stripos("Waiver", $reqstring) !== false) //add all numbers for WAIVER_REC
    {
        $results = array_merge($results, getWaiverValidNum());
    }
    if (stripos("Audio", $reqstring) !== false) //add all numbers for AUDIO_REC
    {
        $results = array_merge($results, getAudioValidNum());
    }
    if (stripos("Documentation", $reqstring) !== false) //add all numbers for DOC_REC
    {
        $results = array_merge($results, getDocumentationValidNum());
    }
    if (stripos("Ready", $reqstring) !== false) //add the only number for READY
    {
        $results = array_merge($results, getReadyValidNum());
    }

    $results = array_keys(array_flip($results)); //remove duplicates in a speedy manner


    return $results;
}