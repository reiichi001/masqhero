<?php

include(dirname(__DIR__) . DIRECTORY_SEPARATOR  . "database.php");
include(dirname(__DIR__) . DIRECTORY_SEPARATOR  . "entries" . DIRECTORY_SEPARATOR  . "status.php");
header("Content-Type: application/json; charset=UTF-8");

session_start();

$query = "SELECT eid, ename, edivision, egroupstatus, erequirements FROM entry WHERE eseason=?";


$columns = array(
    0 => 'eid',
    1 => 'ename',
    2 => 'edivision',
    3 => 'egroupstatus',
    4 => 'erequirements');


$searchwith = $orderby = $limitby = "";

// getting records as per search parameters
/* Filtering */
if ( isset($_POST['search']['value']) && $_POST['search']['value'] != "" ) {
    $searchwith = " AND (";

    $searchstring = $_POST['search']['value'];

    for ( $i=0 ; $i<count($columns) ; $i++ ) {
        $searchwith .= $columns[$i]." LIKE '%".addslashes( $searchstring )."%' OR ";
    }

    //status searching
    $gotstatusnum = getStatusNum($searchstring);

    if (isset($gotstatusnum) && (count($gotstatusnum) > 0)) //we got some valid status
    {
        foreach ($gotstatusnum as $num)
        {
            $searchwith .= $columns[3]." = " . $num . " OR ";
        }

    }

    //requirements searching
    $gotreqsnum = getReqNum($searchstring);

    if (isset($gotreqsnum) && (count($gotreqsnum) > 0)) //we got some valid status
    {
        foreach ($gotreqsnum as $num)
        {
            $searchwith .= $columns[4]." = " . $num . " OR ";
        }

    }


    $searchwith = substr_replace( $searchwith, "", -3 );
    $searchwith .= ')';
}


// sorting
if (isset($_POST['order']) && !empty($_POST['order']))
{
    $orderby = " ORDER BY ". $columns[$_POST['order'][0]['column']] . " " . $_POST['order'][0]['dir'];
}

//view limits
//TODO: Fix pagination. It doesn't display correctly, so we're sending EVERYTHING to process locally
if (isset($_POST['start']) && !empty($_POST['start']) && isset($_POST['length']) && !empty($_POST['length']))
{
    $limitby = " LIMIT ".$_POST['start'] . " ," . $_POST['length']." ";
}
/* END filtering */


try
{
    //grab total results no filter
    $statement = $g_databaseConnection->prepare($query);
    $statement->bind_param('s', $_SESSION['season']);
    $statement->execute();
    $totalData = $statement->get_result()->num_rows;

    //grab results w/ filter
    $query .= $searchwith . $orderby . $limitby;
    error_log($query);

    $statement = $g_databaseConnection->prepare($query);
    if(!$statement)
    {
        throw new Exception(__FUNCTION__ . " failed: " . $g_databaseConnection->error);
    }

    $statement->bind_param('s', $_SESSION['season']);

    if(!$statement->execute())
    {
        throw new Exception(__FUNCTION__ . " failed.");
    }

    $result = $statement->get_result();

    $data = array();

    while ($row = $result->fetch_assoc())
    {
        $data[] = $row;
    }

    $draw = 0;
    if (isset($_POST['draw']))
    {
        $draw = $_POST['draw'];
    }

    $output = ["draw" => $draw,
        "recordsTotal" => $totalData,
        "recordsFiltered" => count($data),
        "data" => $data ];

    echo (json_encode($output));
}
finally
{
    $statement->close();
}