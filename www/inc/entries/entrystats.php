<?php

include(dirname(__DIR__) . DIRECTORY_SEPARATOR  . "database.php");
include(dirname(__DIR__) . DIRECTORY_SEPARATOR  . "entries" . DIRECTORY_SEPARATOR  . "status.php");
header("Content-Type: application/json; charset=UTF-8");

session_start();

$data = array();
$season = $_SESSION['season'];


//Get Count of all entries this event
$query = "SELECT count(eid) AS 'totalcount' FROM entry WHERE eseason='" . $season . "'";
$result = $g_databaseConnection->query($query);

if ($result)
{
    while ($row = $result->fetch_assoc())
    {
        $data[] = $row;
    }
    $result->close();
}

//Get Count of all Division entries this event
$division = "Master";
$query = "SELECT count(edivision) as '" . $division . "' FROM entry WHERE eseason = '" . $season . "' AND edivision = '" . $division . "'";
$result = $g_databaseConnection->query($query);

if ($result)
{
    while ($row = $result->fetch_assoc())
    {
        $data[] = $row;
    }
    $result->close();
}

$division = "Journeyman";
$query = "SELECT count(edivision) as '" . $division . "' FROM entry WHERE eseason = '" . $season . "' AND edivision = '" . $division . "'";
$result = $g_databaseConnection->query($query);

if ($result)
{
    while ($row = $result->fetch_assoc())
    {
        $data[] = $row;
    }
    $result->close();
}

$division = "Novice";
$query = "SELECT count(edivision) as '" . $division . "' FROM entry WHERE eseason = '" . $season . "' AND edivision = '" . $division . "'";
$result = $g_databaseConnection->query($query);

if ($result)
{
    while ($row = $result->fetch_assoc())
    {
        $data[] = $row;
    }
    $result->close();
}

$division = "First Masquerade";
$query = "SELECT count(edivision) as '" . $division . "' FROM entry WHERE eseason = '" . $season . "' AND edivision = '" . $division . "'";
$result = $g_databaseConnection->query($query);

if ($result)
{
    while ($row = $result->fetch_assoc())
    {
        $data[] = $row;
    }
    $result->close();
}





echo (json_encode($data));