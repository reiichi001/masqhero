<?php
include ("inc/database.php");
include ("inc/user/auth.php");
session_start();

$errorMessageDisp = "";

if (isset($_POST['login']))
{
    $errorMessageDisp = setLoginSession($g_databaseConnection);

}

if (checkLogin())
{
    header("location: index.php");
}
else
{
    if(empty($errorMessageDisp)) $errorMessageDisp = "Nobody is logged in";
}


?>
<script language="Javascript">
    function verifyfields() {
        if (document.login.username.value === "") {
            alert("Please enter a username.");
            document.login.username.focus();
            return false;
        }
        if (document.login.password.value === "") {
            alert("Please enter a password.");
            document.login.password.focus();
            return false;
        }

        return true;
    }
</script>

<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>MasqHero User Login</title>
    <link rel="stylesheet" type="text/css" href="css/global.css">

</head>
<body>
<div>
    <form name="login" method="post" onSubmit="return verifyfields();">
        <div class="field-group">
            <div><label for="username">Username</label></div>
            <div><input name="username" type="text" class="input-field"></div>
        </div>
        <div class="field-group">
            <div><label for="password">Password</label></div>
            <div><input name="password" type="password" class="input-field"></div>
        </div>
        <div class="field-group">
            <div><input type="submit" name="login" value="Login" class="form-submit-button"></span></div>
        </div>
    </form>
    <p class="errorMessage"><?php echo $errorMessageDisp; ?></p>
</div>
</body>
</html>