<?php
include ("includes.php");
include("inc/user/auth.php");
session_start();
?>

<?php
//PHP functions start here
if(!empty($_POST["logout"])) { logout(); }

//PHP functions on the page end here
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>MasqedHero Template<?php if (isset($_SESSION['season'])) echo " - " . $_SESSION['season'] ?></title>
    <?php getCSS(); ?>

    <?php getScripts(); ?>

</head>

<body>

<div>
    <nav class="navbar navbar-default navigation-clean-button">
        <div class="container">
            <div class="navbar-header"><a class="navbar-brand" href="#">MasqedHero</a>
                <button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navcol-1"><span
                            class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span
                            class="icon-bar"></span><span class="icon-bar"></span></button>
            </div>
            <div class="collapse navbar-collapse" id="navcol-1">
                <?php
                if (checkLogin() && $_SESSION['gid'] === admin) {
                    ?>
                    <ul class="nav navbar-nav">
                        <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"
                                                href="#">Reports<span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li role="presentation"><a href="#">Problems to address</a></li>
                                <li role="presentation"><a href="#">Judge's Book</a></li>
                                <li role="presentation"><a href="#">Tech Book</a></li>
                                <li role="presentation"><a href="#">MC Book</a></li>
                                <li role="presentation"><a href="#">Order List</a></li>
                                <li role="presentation"><a href="#">Post-Con Report</a></li>
                            </ul>
                        </li>
                    </ul>
                    <form class="navbar-text navbar-right actions" action="" method="post" id="frmLogout">
                        Welcome, <?php echo $_SESSION['username'] ?>
                        <div class="clearfix visible-xs"><br/></div>
                        <input class="btn btn-default action-button" role="button" type="submit" name="logout"
                               value="Logout" style="align: right">
                    </form>
                    <ul class="nav navbar-nav"></ul>
                    <?php
                } else if ( checkLogin() ) {
                    ?>
                    <ul class="nav navbar-nav"></ul>
                    <form class="navbar-text navbar-right actions" action="" method="post" id="frmLogout">
                        Welcome, <?php echo $_SESSION['username'] ?>
                        <div class="clearfix visible-xs"><br/></div>
                        <input class="btn btn-default action-button" role="button" type="submit" name="logout"
                               value="Logout" style="align: right">
                    </form>
                    <ul class="nav navbar-nav"></ul>
                    <?php
                } else {
                    ?>
                    <ul class="nav navbar-nav"></ul>
                    <p class="navbar-text navbar-right actions"><a class="btn btn-default action-button" role="button"
                                                                   href="login.php">Login</a></p>
                    <ul class="nav navbar-nav"></ul>
                    <?php
                }
                ?>

            </div>
        </div>
    </nav>
</div>

<?php
if ( checkLogin() && $_SESSION['gid'] === admin) {
    ?>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">MasqedHero template panel title</h3>
        </div>
        <div class="panel-body">
            <div class="row" style="width:100%;">
                <div class="col-lg-8 col-md-8 show" >
                    <p>When an admin is logged in, they'll see stuff here.</p>
                </div>
            </div>
        </div>
    </div>
    <?php
} else if ( checkLogin() ) {
    echo "Logged in, but nothing to do yet.";
} else {
    include("nologin.php");
}
?>


</body>

</html>