<?php

$title = "Log In";
$path = "";
//include('header.php');
include('inc/database.php');
include('inc/user/add.php');

if (!isset($page)) {
    $page = "";
}
$errorMessageDisp = "";

session_start();

?>
<script language="Javascript">
    function verifyfields() {
        if (document.addUser.username.value === "") {
            alert("Please enter a username.");
            document.addUser.username.focus();
            return false;
        }
        if (document.addUser.password.value === "") {
            alert("Please enter a password.");
            document.addUser.password.focus();
            return false;
        }
        if (document.addUser.password.value === "") {
            alert("Please repeat your password.");
            document.addUser.password.focus();
            return false;
        }
        if (document.addUser.password.value !== document.addUser.repeatpassword.value) {
            alert("Passwords do not match");
            document.addUser.repeatpassword.focus();
            return false;
        }
        if (document.addUser.email.value === "") {
            alert("Please enter an email.");
            document.addUser.email.focus();
            return false;
        }

        return true;
    }
</script>

<script>
    $(document).ready(function () {
        $("#username").blur(function () {
            var jqnew = $("#username").val();
            jqnew = jqnew.replace(/[^a-zA-Z0-9\.@_-]+/g, "");
            $("#username").val(jqnew);
        });
    });
</script>

<script>
    $(document).ready(function () {
        $("#password").blur(function () {
            var jqnew = $("#password").val();
            jqnew = jqnew.replace(/[^a-zA-Z0-9\.@_-]+/g, "");
            $("#password").val(jqnew);
        });

        $("#repeatpassword").blur(function () {
            var jqnew = $("#repeatpassword").val();
            jqnew = jqnew.replace(/[^a-zA-Z0-9\.@_-]+/g, "");
            $("#repeatpassword").val(jqnew);
        });
    });
</script>

<?php
if(isset($_POST["addUser"]))
{
    try
    {
        CheckUserCreateInput($g_databaseConnection);

        $_SESSION['username'] = trim($_POST["username"]);
        header("Location: add_user_success.php");
    }
    catch(Exception $e)
    {

        $errorMessageDisp = $e->getMessage();
        session_destroy();
    }
}

?>

<div>
    <form name="addUser" method="post" onSubmit="return verifyfields();">
        <input type="hidden" name="page" value="<? echo $page ?>">

        <div class="field-group">
            <div><label for="username">Display Name</label></div>
            <div><input name="username" type="text" class="input-field"></div>
        </div>
        <div class="field-group">
            <div><label for="password">Password</label></div>
            <div><input name="password" type="password" class="input-field"></div>
        </div>

        <div class="field-group">
            <div><label for="repeatpassword">Repeat Password</label></div>
            <div><input name="repeatpassword" type="password" class="input-field"></div>
        </div>
        <div class="field-group">
            <div><label for="email">Email</label></div>
            <div><input name="email" type="text" class="input-field"></div>
        </div>
        <div class="field-group">
            <div><input type="submit" name="addUser" value="Create User" class="form-submit-button"></div>
        </div>
    </form>
    <p class="errorMessage"><?php echo $errorMessageDisp; ?></p>
</div>

<?php
//include($path.'footer.php');
?>
